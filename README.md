# README #

## Original Sine: Trigonometry and Sound ##

This project provides software and instructions to conduct a variety of sound
experiments using a computer. It is intended for high school and undergraduate
physics, engineering and mathematics students.

The software provides a 3 channel audio mixer with a waveform (oscilloscope)
and spectrum viewer. It runs on both Windows PCs and Macs.

Note that the software is provided free by the [School of Electrical & Electronic
Engineering](http://eleceng.adelaide.edu.au) at the [University of Adelaide](
http://adelaide.edu.au).

### Getting Started ###

To install under Windows, download `OriginalSineWindowsInstaller.zip` from the
[Downloads page](https://bitbucket.org/bjphillips/original-sine-trigonometry-and-sound/downloads). Uncompress this to a suitable temporary location on your Windows PC and run `setup.exe`.

To install under OSX, download `OriginalSine.dmg` from the
[Downloads page](https://bitbucket.org/bjphillips/original-sine-trigonometry-and-sound/downloads). Open `OriginalSine.dmg` and drag the `OriginalSine.app` icon into the Applications folder.

A set of instructions can be found [here]
(./Distribution/OriginalSine-TrigonometryAndSound.pdf). You may need to click on
`View raw` to download the `pdf` file.

### License ###

This software is distributed under the [BSD License](./LICENSE.md).
Please note that under this license:

> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
> FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
> DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
> SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
> CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
> OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
> OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### Contacts ###

* [Braden Phillips](http://www.adelaide.edu.au/directory/braden.phillips) is the primary project supervisor.