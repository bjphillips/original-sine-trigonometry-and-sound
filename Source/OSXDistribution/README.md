This folder contains the "source files" to make a dmg installation file for the
OriginalSine program.

After trying for too long to do this from the command line, I have resorted to
using [dmgCreator](http://dmgcreator.sourceforge.net/en/).

The aborted command line attempt is archived in Makefile.zip.

dmgCreator Notes
- place the OriginalSine.app icon at (x, y) = (100, 225)
- place the Applications icon at (500,225)